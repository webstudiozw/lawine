<?php

namespace App\Http\Controllers;


use App\Models\CrashLocation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;


class CrashLocationsAPIController extends Controller
{
    public function __construct()
    {



    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'latitude' => ['required'],
            'longitude' => ['required'],
            'client_id' => ['required'],
        ]);
        $location = new CrashLocation();
        $location->client_id = $request->client_id;
        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $location->save();
        return \response()->json([
            'success'=>true,
            'message'=>'Successfully saved'
        ],200);

    }


}
