<?php

namespace App\Http\Controllers;


use App\Models\CrashLocation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;


class CrashLocationsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);


    }

    /**
     * Display a listing of the resource.
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $locations= CrashLocation::with(['client'])
        ->orderBy('created_at', 'desc')
            ->get();
        return Inertia::render('CrashLocations/Index', [
            'locations' => $locations,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('CrashLocations/Create', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
        ]);
        $client = new Client();
        $client->name = $request->name;
        $client->phone_number = $request->phone_number;
        $client->address = $request->address;
        $client->save();
        return redirect()->route('clients.index')->with('success', 'Client saved successfully.');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return \Inertia\Response
     */
    public function show(CrashLocation $location)
    {
        $location->load(['client']);
        return Inertia::render('CrashLocations/Show', [
            'location' => $location,

        ]);
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(CrashLocation $location)
    {
        $location->delete();
        return redirect()->route('clients.index')->with('success', 'Client deleted successfully.');

    }


}
