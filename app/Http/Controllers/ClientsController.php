<?php

namespace App\Http\Controllers;


use App\Models\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;


class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);


    }

    /**
     * Display a listing of the resource.
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $clients = Client::orderBy('created_at', 'desc')
            ->get();
        return Inertia::render('Clients/Index', [
            'clients' => $clients,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Clients/Create', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
        ]);
        $client = new Client();
        $client->name = $request->name;
        $client->phone_number = $request->phone_number;
        $client->address = $request->address;
        $client->save();
        return redirect()->route('clients.index')->with('success', 'Client saved successfully.');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return \Inertia\Response
     */
    public function show(Client $client)
    {
        $client->load(['locations']);
        return Inertia::render('Clients/Show', [
            'client' => $client,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Inertia\Response
     */
    public function edit(Client $client)
    {
        return Inertia::render('Clients/Edit', [
            'client' => $client,
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Client $client
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'name' => ['required'],
        ]);
        $client->name = $request->name;
        $client->phone_number = $request->phone_number;
        $client->address = $request->address;
        $client->save();
        return redirect()->route('clients.index')->with('success', 'Client  updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('clients.index')->with('success', 'Client deleted successfully.');

    }

    public function search(Request $request)
    {
        $search = $request->s;
        $id = $request->id;
        $data = Client::with(['category'])
            ->where(function ($query) use ($search) {
                $query->where('first_name', 'like', "%$search%");
                $query->orWhere('last_name', 'like', "%$search%");
                $query->orWhere('name', 'like', "%$search%");
                $query->orWhere('middle_name', 'like', "%$search%");
                $query->orWhere('id', 'like', "%$search%");
                $query->orWhere('email', 'like', "%$search%");
                $query->orWhere('mobile', 'like', "%$search%");
                $query->orWhere('external_id', 'like', "%$search%");
            })->when($id, function ($query) use ($id) {
                return $query->where('id', $id);
            })->get();
        return response()->json($data);
    }

    public function vehicles(Request $request, Client $client)
    {
        $vehicles = Vehicle::with(['client', 'transmissionType', 'fuelType', 'driveType', 'bodyType', 'make'])
            ->filter(\request()->only('search', 'client_id', 'vehicle_make_id', 'vehicle_body_type_id', 'vehicle_drive_type_id', 'vehicle_fuel_type_id', 'vehicle_transmission_type_id', 'date_range'))
            ->where('client_id', $client->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return Inertia::render('Clients/Vehicles/Index', [
            'filters' => \request()->all('search', 'client_type', 'client_category_id', 'currency_id', 'branch_id', 'date_range'),
            'client' => $client,
            'vehicles' => $vehicles,
            'makes' => VehicleMake::get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name,
                ];
            }),
            'driveTypes' => VehicleDriveType::get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name
                ];
            }),
            'transmissionTypes' => VehicleTransmissionType::get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name
                ];
            }),
            'bodyTypes' => VehicleBodyType::get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name
                ];
            }),
            'fuelTypes' => VehicleFuelType::get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name
                ];
            }),
        ]);
    }

    public function invoices(Request $request, Client $client)
    {
        $invoices = Invoice::with(['client', 'invoiceItems', 'currency'])
            ->where('client_id', $client->id)
            ->filter(\request()->only('search', 'status', 'client_id', 'staff_id', 'date_range', 'currency_id'))
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return Inertia::render('Clients/Invoices/Index', [
            'filters' => \request()->all('search', 'status', 'client_id', 'staff_id', 'date_range', 'currency_id'),
            'client' => $client,
            'invoices' => $invoices,
            'currencies' => Currency::where('active', 1)->get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name
                ];
            }),
        ]);
    }

    public function workOrders(Request $request, Client $client)
    {
        $orders = WorkOrder::with(['client', 'vehicle', 'invoice', 'createdBy', 'assignedTo'])
            ->where('client_id', $client->id)
            ->filter(\request()->only('search', 'client_id', 'assigned_to_id', 'vehicle_id', 'created_by_id', 'status', 'currency_id', 'staff_id', 'date_range'))
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return Inertia::render('Clients/WorkOrders/Index', [
            'filters' => \request()->all('search', 'client_id', 'assigned_to_id', 'vehicle_id', 'created_by_id', 'status', 'currency_id', 'staff_id', 'date_range'),
            'client' => $client,
            'orders' => $orders,
            'currencies' => Currency::where('active', 1)->get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name
                ];
            }),
        ]);
    }

    public function payments(Request $request, Client $client)
    {
        $invoicePayments = InvoicePayment::with(['client', 'currency', 'paymentType', 'invoice'])
            ->where('client_id', $client->id)
            ->filter(\request()->only('search', 'client_id', 'date_range', 'currency_id'))
            ->paginate(20);
        return Inertia::render('Clients/Payments/Index', [
            'filters' => \request()->all('search', 'client_id', 'date_range', 'currency_id'),
            'client' => $client,
            'invoicePayments' => $invoicePayments,
            'currencies' => Currency::where('active', 1)->get()->map(function ($item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name
                ];
            }),
        ]);
    }

    public function files(Request $request, Client $client)
    {
        $files = File::where('record_id', $client->id)
            ->where('category', 'clients')
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return Inertia::render('Clients/Files/Index', [
            'filters' => \request()->all('search', 'client_id', 'date_range', 'currency_id'),
            'client' => $client,
            'files' => $files,
        ]);
    }

    public function createFile(Request $request, Client $client)
    {

        return Inertia::render('Clients/Files/Create', [
            'client' => $client,
        ]);
    }

    public function storeFile(Request $request, Client $client)
    {

        $fileController = new FilesController();
        $file = $fileController->store([
            'file' => $request->file('file'),
            'name' => $request->name,
            'description' => $request->description,
            'category' => 'clients',
            'record_id' => $client->id,
        ]);
        activity()
            ->performedOn($client)
            ->log('Create Client File');
        return redirect()->route('clients.files.index', [$client->id])->with('success', 'File created successfully.');

    }

    public function destroyFile(File $file)
    {
        $fileController = new FilesController();
        $fileController->destroy($file->id);
        activity()
            ->performedOn($file)
            ->log('Delete Client File');
        return redirect()->back()->with('success', 'File deleted successfully.');

    }

    /**
     * Show the form for editing the specified resource.
     * @param File $file
     * @return \Inertia\Response
     */
    public function editFile(File $file)
    {
        $client = Client::find($file->record_id);
        return Inertia::render('Clients/Files/Edit', [
            'client' => $client,
            'file' => $file,
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param WorkOrder $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateFile(Request $request, File $file)
    {

        $client = Client::find($file->record_id);
        $fileController = new FilesController();
        $file = $fileController->update($file->id, [
            'file' => $request->file('file'),
            'name' => $request->name,
            'description' => $request->description,
        ]);
        activity()
            ->performedOn($client)
            ->log('Update Client File');
        return redirect()->route('clients.files.index', [$client->id])->with('success', 'Updated File successfully.');

    }
}
