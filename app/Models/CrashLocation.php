<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrashLocation extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'client_id',
        'latitude',
        'longitude',
        'description',
    ];
protected $casts=[
    'latitude'=>'double',
    'longitude'=>'double',
];
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
