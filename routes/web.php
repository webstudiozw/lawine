<?php

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\CrashLocationsController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});
//clients
Route::group([
    'prefix' => 'client',
    'as' => 'clients.',
], function () {
    Route::get('/', [ClientsController::class, 'index'])->name('index');
    Route::get('/create', [ClientsController::class, 'create'])->name('create');
    Route::post('/store', [ClientsController::class, 'store'])->name('store');
    Route::get('/{client}/show', [ClientsController::class, 'show'])->name('show');
    Route::get('/{client}/edit', [ClientsController::class, 'edit'])->name('edit');
    Route::put('/{client}/update', [ClientsController::class, 'update'])->name('update');
    Route::delete('/{client}/destroy', [ClientsController::class, 'destroy'])->name('destroy');
});
//crash locations
Route::group([
    'prefix' => 'crash_location',
    'as' => 'crash_locations.',
], function () {
    Route::get('/', [CrashLocationsController::class, 'index'])->name('index');
    Route::get('/create', [CrashLocationsController::class, 'create'])->name('create');
    Route::post('/store', [CrashLocationsController::class, 'store'])->name('store');
    Route::get('/{location}/show', [CrashLocationsController::class, 'show'])->name('show');

    Route::delete('/{location}/destroy', [CrashLocationsController::class, 'destroy'])->name('destroy');
});
