#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <AltSoftSerial.h>

const int clientID = 1;  //the client in our system
const int GSMRXPin = 5, GSMTXPin = 4;
const int trigPin = 2;
const int echoPin = 3;
const int buzzerPin = 10;
const String phoneNumber = "+263774175438";
const char apn = "econet.net";
long duration;
int distance;
TinyGPSPlus gps;
SoftwareSerial sim800L(GSMRXPin, GSMTXPin);
//GPS Module RX pin to Arduino 9
//GPS Module TX pin to Arduino 8
AltSoftSerial neogps;
unsigned long previousMillis = 0;
long interval = 60000;
void setup() {

  //Begin serial communication with Arduino and SIM800L
  sim800L.begin(9600);
  //Begin serial communication with Arduino and gpd module
  neogps.begin(9600);
  Serial.begin(9600);
  pinMode(buzzerPin, OUTPUT);
  pinMode(trigPin, OUTPUT);  // Sets the trigPin as an Output
  pinMode(echoPin, INPUT);   // Sets the echoPin as an Input
  Serial.println("Initializing...");
  //delay(10000);
  //Once the handshake test is successful, it will back to OK
  sendATcommand("AT", "OK", 2000);
  sendATcommand("AT+CMGF=1", "OK", 2000);
  //sim800L.print("AT+CMGR=40\r");
  Serial.println("Initialized");
}

void loop() {
  int currentDistance = 300;
  while (currentDistance >= 2)  //we crash at 5cm, adjust to zero if need be
  {
    Serial.print("Current Distance: ");
    Serial.println(currentDistance);
    currentDistance = getDistance();
  }
  tone(buzzerPin, 100);
  delay(2000);
  noTone(buzzerPin);
  boolean dataSent = false;
  //send gsm data to server
  while (dataSent == false) {
    dataSent = sendGpsToServer();
    if (dataSent == 1) {
      Serial.println("Data sent to server");
      //also place a call or send sms
      SendTextMessage();
      DialVoiceCall();
      delay(10000);
    }
  }
  Serial.println("Complete");
  
}
int getDistance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2;
  // Prints the distance on the Serial Monitor
  //Serial.print("Distance: ");
  //Serial.println(distance);
  return distance;
}
boolean sendGpsToServer() {
  //Can take up to 60 seconds
  boolean newData = false;
  for (unsigned long start = millis(); millis() - start < 2000;) {
    while (neogps.available()) {
      if (gps.encode(neogps.read())) {
        newData = true;
        break;
      }
    }
  }

  //If newData is true
  if (newData) {
    newData = false;

    String latitude, longitude;
    float altitude;
    unsigned long date, time, speed, satellites;

    latitude = String(gps.location.lat(), 6);   // Latitude in degrees (double)
    longitude = String(gps.location.lng(), 6);  // Longitude in degrees (double)
    altitude = gps.altitude.meters();           // Altitude in meters (double)
    date = gps.date.value();                    // Raw date in DDMMYY format (u32)
    time = gps.time.value();                    // Raw time in HHMMSSCC format (u32)
    speed = gps.speed.kmph();

    Serial.print("Latitude= ");
    Serial.print(latitude);
    Serial.print(" Longitude= ");
    Serial.println(longitude);

    if (latitude == 0) { return 0; }

    String url, temp;
    url = "http://lawine.webstudio.co.zw/api/store_location?latitude=";
    url += latitude;
    url += "&longitude=";
    url += longitude;
    url += "&client_id=";
    url += clientID;


    Serial.println(url);
    delay(300);

    sendATcommand("AT+CFUN=1", "OK", 2000);
    //AT+CGATT = 1 Connect modem is attached to GPRS to a network. AT+CGATT = 0, modem is not attached to GPRS to a network
    sendATcommand("AT+CGATT=1", "OK", 2000);
    //Connection type: GPRS - bearer profile 1
    sendATcommand("AT+SAPBR=3,1,\"Contype\",\"GPRS\"", "OK", 2000);
    //sets the APN settings for your network provider.
    sendATcommand("AT+SAPBR=3,1,\"APN\",\"internet.netone\"", "OK", 2000);
    //enable the GPRS - enable bearer 1
    sendATcommand("AT+SAPBR=1,1", "OK", 2000);
    //Init HTTP service
    sendATcommand("AT+HTTPINIT", "OK", 2000);
    sendATcommand("AT+HTTPPARA=\"CID\",1", "OK", 1000);
    sim800L.print("AT+HTTPPARA=\"URL\",\"");
    sim800L.print(url);
    sendATcommand("\"", "OK", 1000);
    //Set up the HTTP action
    sendATcommand("AT+HTTPACTION=0", "0,200", 1000);
    //Terminate the HTTP service
    sendATcommand("AT+HTTPTERM", "OK", 1000);
    //shuts down the GPRS connection. This returns "SHUT OK".
    sendATcommand("AT+CIPSHUT", "SHUT OK", 1000);
  }
  return true;
}

int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout) {

  uint8_t x = 0, answer = 0;
  char response[100];
  unsigned long previous;

  //Initialice the string
  memset(response, '\0', 100);
  delay(100);

  //Clean the input buffer
  while (sim800L.available() > 0) sim800L.read();

  if (ATcommand[0] != '\0') {
    //Send the AT command
    sim800L.println(ATcommand);
  }

  x = 0;
  previous = millis();

  //this loop waits for the answer with time out
  do {
    //if there are data in the UART input buffer, reads it and checks for the asnwer
    if (sim800L.available() != 0) {
      response[x] = sim800L.read();
      //Serial.print(response[x]);
      x++;
      // check if the desired answer (OK) is in the response of the module
      if (strstr(response, expected_answer) != NULL) {
        answer = 1;
      }
    }
  } while ((answer == 0) && ((millis() - previous) < timeout));

  Serial.println(response);
  return answer;
}
void SendTextMessage() {
  Serial.println("Sending Text...");
  sim800L.print("AT+CMGF=1\r");  // Set the shield to SMS mode
  delay(100);
  String latitude, longitude;
  latitude = String(gps.location.lat(), 6);   // Latitude in degrees (double)
  longitude = String(gps.location.lng(), 6);  // Longitude in degrees (double)
  sim800L.print("AT+CMGS=\"" + phoneNumber + "\"\r");
  delay(200);
  sim800L.print("Possible crash http://maps.google.com/?q=");
  sim800L.print(latitude);
  sim800L.print(",");
  sim800L.print(longitude);
  sim800L.print("\r");  //the content of the message
  delay(500);
  sim800L.print((char)26);  //the ASCII code of the ctrl+z is 26 (required according to the datasheet)
  delay(100);
  sim800L.println();
  Serial.println("Text Sent.");
  delay(500);
}

void DialVoiceCall() {
  Serial.println("Calling desginated number...");
  sim800L.println("ATD" + phoneNumber + ";");  //dial the number, must include country code
  // sim800L.print("ATD");
  // sim800L.print(phoneNumber);
  // sim800L.print(";");
  //  sim800L.println();
  delay(100);
  sim800L.println();
}